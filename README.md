# AdventOfCodeDay3

My solution for AOC2020 day three in Java. I used Processing to visualise the problem.

![Output of my program.](treesAoC.png)