import java.io.*;  


class Trees {
  private int lineLen;
  public Trees() {
    this.lineLen = 31;
  }
  public void exec() {
    try {
      File file=new File(dataPath(".."), "trees.txt"); // absolute path on my pc, sorry lol
      FileReader fr=new FileReader(file);   //reads the file  

      BufferedReader br = new BufferedReader(fr);  //creates a buffering character input stream  
      String line;  
      int x, y;
      x = 0;
      y = 0;
      int player = 0;
      int collisions = 0;
      int lineNum = 0;

      while ((line=br.readLine())!=null) {
        x = 0;
        y = y+10;
        lineNum = lineNum + 1;
        String[]data=line.split("");

        for (int i = 0; i < this.lineLen; i++) {
          x= x+10;

          if (data[i].equals (".")) {
            if (player == i) {
              fill(255);
              rect(x-5, y-5, 10, 10);
            }
          }
          if (data[i].equals("#")) {
            if (player == i) {

              fill(255, 0, 0); // red square
              rect(x-5, y-5, 10, 10);
              collisions = collisions + 1;
            } else {
              fill(0, 255, 0);
              ellipse(x, y, 10, 10);
            }
          }
        }
        player = player+3;

        if (player >= this.lineLen) {
          player = player - lineLen;
        }
      }  

      fr.close();
      println(collisions);
    }
    catch(IOException e) {

      e.printStackTrace();
    }
  }
}
size(900, 900);
Trees trees = new Trees();
trees.exec();
